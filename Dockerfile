FROM ansibleplaybookbundle/apb-base

LABEL "com.redhat.apb.spec"=\
"dmVyc2lvbjogJzEuMC4wJwpuYW1lOiBkcnVwYWwtYXBiCmRlc2NyaXB0aW9uOiB5b3VyIGRlc2Ny\
aXB0aW9uCmJpbmRhYmxlOiBGYWxzZQphc3luYzogb3B0aW9uYWwKbWV0YWRhdGE6CiAgZGlzcGxh\
eU5hbWU6IGRydXBhbC1hcGIKcGxhbnM6CiAgLSBuYW1lOiBkZWZhdWx0CiAgICBkZXNjcmlwdGlv\
bjogVGhpcyBkZWZhdWx0IHBsYW4gZGVwbG95cyBkcnVwYWwtYXBiCiAgICBmcmVlOiBUcnVlCiAg\
ICBtZXRhZGF0YToge30KICAgIHBhcmFtZXRlcnM6IFtd"

RUN yum install -y \
    postgresql-libs \
    && pip install psycopg2 \
    && yum clean all    

COPY playbooks /opt/apb/project/
COPY roles /opt/ansible/roles/

RUN chmod -R g=u /opt/{ansible,apb}
USER apb