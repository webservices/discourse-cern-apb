##############################################################################
## Provision Discourse APB
## This role executes much of the needed functionality to provision an
## application using an Ansible Playbook Bundle.  Included in the comments
## below are some sample resources for getting started deploying an application
## to OpenShift.
##############################################################################
 
##############################################################################
## Provision Redis
##############################################################################
- name: Create redis deployment config
  openshift_v1_deployment_config:
    name: redis
    namespace: '{{ namespace }}'
    state: present
    resource_definition:
      metadata:
        labels:
          app: discourse-cern
        name: redis
        namespace: '{{ namespace }}'
      spec:
        replicas: 1
        selector:
          app: discourse-cern
          deploymentconfig: redis
        strategy:
          type: Rolling
        template:
          metadata:
            labels:
              app: discourse-cern
              deploymentconfig: redis
          spec:
            containers:
            - image: ' '
              imagePullPolicy: Always
              name: redis
              ports:
              - containerPort: 6379
                protocol: TCP
              resources: {}
              terminationMessagePath: /dev/termination-log
              volumeMounts:
              - mountPath: /var/lib/redis/data
                name: redis-1
              readinessProbe:
                failureThreshold: 3
                initialDelaySeconds: 5
                periodSeconds: 10
                successThreshold: 1
                tcpSocket:
                  port: 6379
                timeoutSeconds: 10
              livenessProbe:
                failureThreshold: 3
                initialDelaySeconds: 30
                periodSeconds: 10
                successThreshold: 1
                tcpSocket:
                  port: 6379
                timeoutSeconds: 10
            dnsPolicy: ClusterFirst
            restartPolicy: Always
            securityContext: {}
            volumes:
            - emptyDir: {}
              name: redis-1
        test: false
        triggers:
        - type: ConfigChange
        - type: ImageChange
          imageChangeParams:
            automatic: true
            containerNames:
              - "redis"
            from:
              kind: "ImageStreamTag"
              name: 'redis:3.2'
              namespace: openshift

- name: Create redis service
  k8s_v1_service:
    name: redis
    namespace: '{{ namespace }}'
    state: present
    labels:
      app: discourse-cern
      service: redis
    selector:
      app: discourse-cern
      deploymentconfig: redis
    ports:
    - name: 6379-tcp
      port: 6379
      protocol: TCP
      targetPort: 6379
    type: ClusterIP

# wait for deployment to be ready before we can configure Discourse
- name: wait for redis deployment to complete
  shell: oc rollout status dc/redis --watch=false -n {{ namespace | quote }}
  register: oc_rollout_status
  until: ' "successfully rolled out" in oc_rollout_status.stdout '
  retries: 60
  delay: 10

##############################################################################
## Provision Common Discourse
##############################################################################
###################
# CONFIGMAPS      #
###################
- name: Create global-env configmap
  k8s_v1_config_map:
    name: env-configmap
    namespace: '{{ namespace }}'
    state: present
    labels:
      app: discourse-cern
    data:
      DISCOURSE_DB_HOST_KEY: "{{ db_host_value }}"
      DISCOURSE_DB_PORT_KEY: "{{ db_port_value }}"
      DISCOURSE_DB_NAME_KEY: "{{ db_name_value }}"
      DISCOURSE_DB_USERNAME_KEY: "{{ db_username_value }}"
      DISCOURSE_DB_PASSWORD_KEY: "{{ db_password_value }}"
      DISCOURSE_DEVELOPER_EMAILS_KEY: "{{ apb_discourse_developer_emails }},{{ developer_email_default_account }}"

- name: Create discourse configmap
  k8s:
    name: discourse-configmap
    namespace: '{{ namespace }}'
    resource_definition: "{{ lookup('file', 'discourse-configmap.yml') }}"

###################
# PVC             #
###################
- name: Create discourse uploads pvc
  k8s_v1_persistent_volume_claim:
    name: discourse-uploads
    namespace: '{{ namespace }}'
    state: present
    access_modes:
      - ReadWriteOnce
    resources_requests:
      storage: 5Gi

##############################################################################
## Provision Discourse (webapp)
##############################################################################
###################
# DEPLOYMENTS     #
###################
- name: Create discourse (webapp) deployment config
  openshift_v1_deployment_config:
    name: webapp
    namespace: '{{ namespace }}'
    state: present
    resource_definition:
      metadata:
        labels:
          app: discourse-cern
        name: webapp
      spec:
        replicas: 1
        selector:
          app: discourse-cern
          deploymentconfig: webapp
        strategy:
          rollingParams:
            timeoutSeconds: 1200
          type: Rolling
        template:
          metadata:
            labels:
              app: discourse-cern
              deploymentconfig: webapp
          spec:
            containers:
            -
              name: webapp
              image: gitlab-registry.cern.ch/webservices/discourse-cern:stable
              imagePullPolicy: IfNotPresent
              ports:
              - containerPort: 8080
                protocol: TCP
              resources:
                limits:
                  # limit as per https://github.com/discourse/discourse/blob/master/docs/ADMIN-QUICK-START-GUIDE.md#maintenance
                  memory: 1Gi
                  cpu: 1
                requests:
                  memory: 320Mi
                  cpu: 200m
              terminationMessagePath: /dev/termination-log
              volumeMounts:
              - mountPath: /tmp/discourse-configmap
                name: discourse-configmap
              - mountPath: /discourse/public/uploads
                name: discourse-uploads
              - mountPath: /discourse/public/assets
                name: discourse-public-assets
              readinessProbe:
                failureThreshold: 3
                initialDelaySeconds: 30
                periodSeconds: 10
                successThreshold: 1
                tcpSocket:
                  port: 8080
                timeoutSeconds: 10
              livenessProbe:
                failureThreshold: 3
                initialDelaySeconds: 900
                periodSeconds: 10
                successThreshold: 1
                tcpSocket:
                  port: 8080
                timeoutSeconds: 10
              env:
                - name: NAMESPACE
                  valueFrom:
                    fieldRef:
                      apiVersion: v1
                      fieldPath: metadata.namespace
                - name: HOSTNAME
                  value: $(NAMESPACE).web.cern.ch
                - name: DISCOURSE_CONFIG_DB_HOST
                  valueFrom:
                    configMapKeyRef:
                      key: DISCOURSE_DB_HOST_KEY
                      name: env-configmap
                - name: DISCOURSE_CONFIG_DB_PORT
                  valueFrom:
                    configMapKeyRef:
                      key: DISCOURSE_DB_PORT_KEY
                      name: env-configmap
                - name: DISCOURSE_CONFIG_DB_NAME
                  valueFrom:
                    configMapKeyRef:
                      key: DISCOURSE_DB_NAME_KEY
                      name: env-configmap
                - name: DISCOURSE_CONFIG_DB_USERNAME
                  valueFrom:
                    configMapKeyRef:
                      key: DISCOURSE_DB_USERNAME_KEY
                      name: env-configmap
                - name: DISCOURSE_CONFIG_DB_PASSWORD
                  valueFrom:
                    configMapKeyRef:
                      key: DISCOURSE_DB_PASSWORD_KEY
                      name: env-configmap
                - name: DISCOURSE_CONFIG_DEVELOPER_EMAILS
                  valueFrom:
                    configMapKeyRef:
                      key: DISCOURSE_DEVELOPER_EMAILS_KEY
                      name: env-configmap
                - name: PUMA_WORKERS
                  value: '1'

            dnsPolicy: ClusterFirst
            restartPolicy: Always
            securityContext: {}

            initContainers:
              - name: init-dbmigration
                command:
                  - ./init-dbmigration.sh
                env:
                  - name: NAMESPACE
                    valueFrom:
                      fieldRef:
                        apiVersion: v1
                        fieldPath: metadata.namespace
                  - name: HOSTNAME
                    value: $(NAMESPACE).web.cern.ch
                  - name: DISCOURSE_CONFIG_DB_HOST
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DB_HOST_KEY
                        name: env-configmap
                  - name: DISCOURSE_CONFIG_DB_PORT
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DB_PORT_KEY
                        name: env-configmap
                  - name: DISCOURSE_CONFIG_DB_NAME
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DB_NAME_KEY
                        name: env-configmap
                  - name: DISCOURSE_CONFIG_DB_USERNAME
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DB_USERNAME_KEY
                        name: env-configmap
                  - name: DISCOURSE_CONFIG_DB_PASSWORD
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DB_PASSWORD_KEY
                        name: env-configmap
                  - name: DISCOURSE_CONFIG_DEVELOPER_EMAILS
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DEVELOPER_EMAILS_KEY
                        name: env-configmap
                image: gitlab-registry.cern.ch/webservices/discourse-cern:stable
                imagePullPolicy: IfNotPresent
                resources:
                  limits:
                    cpu: '1'
                    memory: 1Gi
                  requests:
                    cpu: 200m
                    memory: 320Mi
                volumeMounts:
                  - mountPath: /tmp/discourse-configmap
                    name: discourse-configmap

              - name: init-assets
                command:
                  - ./init-assets.sh
                env:
                  - name: NAMESPACE
                    valueFrom:
                      fieldRef:
                        apiVersion: v1
                        fieldPath: metadata.namespace
                  - name: HOSTNAME
                    value: $(NAMESPACE).web.cern.ch
                  - name: DISCOURSE_CONFIG_DB_HOST
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DB_HOST_KEY
                        name: env-configmap
                  - name: DISCOURSE_CONFIG_DB_PORT
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DB_PORT_KEY
                        name: env-configmap
                  - name: DISCOURSE_CONFIG_DB_NAME
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DB_NAME_KEY
                        name: env-configmap
                  - name: DISCOURSE_CONFIG_DB_USERNAME
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DB_USERNAME_KEY
                        name: env-configmap
                  - name: DISCOURSE_CONFIG_DB_PASSWORD
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DB_PASSWORD_KEY
                        name: env-configmap
                  - name: DISCOURSE_CONFIG_DEVELOPER_EMAILS
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DEVELOPER_EMAILS_KEY
                        name: env-configmap
                image: gitlab-registry.cern.ch/webservices/discourse-cern:stable
                imagePullPolicy: IfNotPresent
                resources:
                  limits:
                    cpu: '1'
                    memory: 1Gi
                  requests:
                    cpu: 200m
                    memory: 320Mi
                volumeMounts:
                  - mountPath: /tmp/discourse-configmap
                    name: discourse-configmap
                  - mountPath: /discourse/public/assets
                    name: discourse-public-assets

            volumes:
            - name: discourse-configmap
              configMap:
                name: discourse-configmap
            - name: discourse-public-assets
              emptyDir: {}
            - name: discourse-uploads
              persistentVolumeClaim:
                claimName: discourse-uploads
        triggers:
        - type: ConfigChange
        - type: ImageChange
          imageChangeParams:
            automatic: true
            containerNames:
            - "init-dbmigration"
            - "init-assets"
            - "webapp"
            from:
              kind: ImageStreamTag
              name: discourse-cern:stable
              namespace: openshift

###################
# SERVICES        #
###################
- name: Create webapp service
  k8s_v1_service:
    name: webapp
    namespace: '{{ namespace }}'
    state: present
    labels:
      app: discourse-cern
      service: webapp
    selector:
      app: discourse-cern
      deploymentconfig: webapp
    ports:
      - name: 8080-tcp
        port: 8080
        target_port: 8080


# wait for deployment to be ready before we can configure Sidekiq
# - name: wait for webapp deployment to complete
#   shell: oc rollout status dc/webapp --watch=false -n {{ namespace | quote }}
#   register: oc_rollout_status
#   until: ' "successfully rolled out" in oc_rollout_status.stdout '
#   retries: 60
#   delay: 10

##############################################################################
## Provision Sidekiq
##############################################################################
- name: Create sidekiq deployment config
  openshift_v1_deployment_config:
    name: sidekiq
    namespace: '{{ namespace }}'
    state: present
    resource_definition:
      metadata:
        labels:
          app: discourse-cern
        name: sidekiq
      spec:
        replicas: 1
        selector:
          app: discourse
          deploymentconfig: sidekiq
        strategy:
          type: Rolling
        template:
          metadata:
            labels:
              app: discourse
              deploymentconfig: sidekiq
          spec:
            containers:
            -
              name: sidekiq
              image: gitlab-registry.cern.ch/webservices/discourse-cern:stable
              imagePullPolicy: IfNotPresent
              command: # sidekiq container has a different entrypoint
                  - ./run-sidekiq.sh
              ports:
              - containerPort: 8080
                protocol: TCP
              resources:
                limits:
                  memory: 800Mi
                  cpu: "1"
                requests:
                  memory: 400Mi
                  cpu: 200m
              terminationMessagePath: /dev/termination-log
              volumeMounts:
              - mountPath: /tmp/discourse-configmap
                name: discourse-configmap
              - mountPath: /discourse/public/uploads
                name: discourse-uploads
              readinessProbe:
                exec:
                  command:
                    - cat
                    - /discourse/tmp/pids/sidekiq.pid
                failureThreshold: 3
                initialDelaySeconds: 60
                periodSeconds: 10
                successThreshold: 1
                timeoutSeconds: 10
              livenessProbe:
                exec:
                  command:
                    - cat
                    - /discourse/tmp/pids/sidekiq.pid
                failureThreshold: 3
                initialDelaySeconds: 300
                periodSeconds: 10
                successThreshold: 1
                timeoutSeconds: 10
              env:
                  - name: "NAMESPACE"
                    valueFrom:
                      fieldRef:
                        apiVersion: v1
                        fieldPath: metadata.namespace
                  - name: HOSTNAME
                    value: "$(NAMESPACE).web.cern.ch"
                  - name: DISCOURSE_CONFIG_DB_HOST
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DB_HOST_KEY
                        name: env-configmap
                  - name: DISCOURSE_CONFIG_DB_PORT
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DB_PORT_KEY
                        name: env-configmap
                  - name: DISCOURSE_CONFIG_DB_NAME
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DB_NAME_KEY
                        name: env-configmap
                  - name: DISCOURSE_CONFIG_DB_USERNAME
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DB_USERNAME_KEY
                        name: env-configmap
                  - name: DISCOURSE_CONFIG_DB_PASSWORD
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DB_PASSWORD_KEY
                        name: env-configmap
                  - name: DISCOURSE_CONFIG_DEVELOPER_EMAILS
                    valueFrom:
                      configMapKeyRef:
                        key: DISCOURSE_DEVELOPER_EMAILS_KEY
                        name: env-configmap

            dnsPolicy: ClusterFirst
            restartPolicy: Always
            securityContext: {}                
            volumes:
            - name: discourse-configmap
              configMap:
                name: discourse-configmap
            - name: discourse-uploads
              persistentVolumeClaim:
                claimName: discourse-uploads
        triggers:
        - type: "ConfigChange"
        - type: ImageChange
          imageChangeParams:
            automatic: true
            containerNames:
            - "sidekiq"
            from:
              kind: "ImageStreamTag"
              name: "discourse-cern:stable"
              namespace: openshift


###################
# ROUTES          #
###################
- name: Create route
  openshift_v1_route:
    name: webapp
    namespace: '{{ namespace }}'
    state: present
    labels:
      app: discourse-cern
    state: present
    spec_port_target_port: 8080-tcp
    spec_to_kind: Service
    spec_to_name: webapp
    tls_termination: edge
    spec_tls_insecure_edge_termination_policy: Redirect